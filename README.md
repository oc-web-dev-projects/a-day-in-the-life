# A Day in the Life of a Web Developer

1st project of the OpenClassrrooms' path: Web Developer.

> What will your job look like? Get a peek at what it's actually like to work as a web developer! You'll analyze the job and define a learning strategy for working towards becoming an autonomous professional.

🔧 Skills acquired in this project:

- Describe the role of a web developer
- Develop a learning plan to develop the required skills for a developer
- Select pertinent sources for a technology watch

